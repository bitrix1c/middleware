<?php

declare(strict_types = 1);

namespace Some\Module\Http;

use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Routing\Options;
use Some\Module\Http\Controller\BaseController;

/**
 * @property array $middleware свойство в котором хранится список мидлвейров, переданных
 * через метод RoutingConfigurator::middleware()
 *
 * @author Антон Егоров
 */
class RouterOptions extends Options
{
    /**
     * Добавляем метод, который формирует список ActionFilter'ов для передачи в контроллер
     *
     * @see BaseController::getDefaultPreFilters();
     * @return array
     */
    public function getActionFiltersFromMiddleware(): array
    {
        $actionFilterList = [];
        foreach ($this->middleware as $middleware) {
            if (class_exists($middleware) && $middleware instanceof ActionFilter\Base) {
                $actionFilterList[] = new $middleware;
            }
        }

        return $actionFilterList;
    }
}
