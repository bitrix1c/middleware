<?php

declare(strict_types = 1);

namespace Some\Module\Http\ActionFilter;

use Bitrix\Main\Engine\ActionFilter;
use Bitrix\Main\Event;

/**
 * @author Антон Егоров
 */
class SomeMiddleware extends ActionFilter\Base
{
    /**
     * Реализация middleware в соответствии документацией по ActionFilter
     *
     * @link https://dev.1c-bitrix.ru/api_d7/bitrix/main/engine/actionfilter/index.php
     * @param Event $event
     * @return void
     */
    public function onBeforeAction(Event $event)
    {
        //...
    }
}