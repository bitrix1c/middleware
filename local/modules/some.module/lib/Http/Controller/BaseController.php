<?php

declare(strict_types = 1);

namespace Some\Module\Http\Controller;

use Bitrix\Main\Application;
use Bitrix\Main\Engine\Controller;
use Bitrix\Main\HttpRequest;
use Some\Module\Http\RouterOptions;

/**
 * Базовый класс для всех контроллеров
 * @link https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&CHAPTER_ID=03750&LESSON_PATH=3913.3516.5062.3750
 *
 * @method HttpRequest getRequest() метод контроллера возвращает объект запроса
 * @author Антон Егоров
 */
abstract class BaseController extends Controller
{
    /**
     * Переопределяет стандартный метод и добавляем в возвращаемый им массив префильтры, которые переданы в метод
     * RoutingConfigurator::middleware() соответствующего конечному контроллеру роуту или группе роутов
     *
     * @return array
     */
    protected function getDefaultPreFilters(): array
    {
        $options = new RouterOptions();
        $options->mergeWith(Application::getInstance()->getRouter()->match($this->getRequest())->getOptions());

        return array_merge(parent::getDefaultPreFilters(), $options->getActionFiltersFromMiddleware());
    }
}