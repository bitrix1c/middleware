<?php

declare(strict_types = 1);

namespace Some\Module\Http\Controller;

/**
 * Пример контроллера для экшена которого будет запущен ActionFilter, название класса которого мы передали в метод
 * RoutingConfigurator::middleware()
 *
 * @link https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=43&CHAPTER_ID=03750&LESSON_PATH=3913.3516.5062.3750
 * @author Антон Егоров
 */
class ApiController extends BaseController
{
    /**
     * Экшен контроллера для примера
     * @return string[]
     */
    public function methodAction(): array
    {
        return ['actionName' => 'method'];
    }
}