<?php

use Bitrix\Main\Routing\RoutingConfigurator;
use Some\Module\Http\ActionFilter\SomeMiddleware;
use Some\Module\Http\Controller\ApiController;

return static function (RoutingConfigurator $routes) {
    /** Пример использования в роуте */
    $routes->get('/api/method', [ApiController::class, 'methodAction'])->middleware(SomeMiddleware::class);

    /** Пример использования для группы роутов */
    $routes->group(function (RoutingConfigurator $routes) {
        $routes->get('/api/method', [ApiController::class, 'methodAction']);
    })->middleware(SomeMiddleware::class);
};